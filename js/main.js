
$(function() {
    var pull = $('.header_toggle');
    var box = $('.page');

    $(pull).on('click', function(e) {
        e.preventDefault();
        box.toggleClass('nav_open');
    });
});

// IE11 SVG support
svg4everybody();
